//Sample Data-Students
var students=[{id:1,name:"midhun",email:"something@sm.com"},{id:2,name:"midhun",email:"something@sm.com"},{id:3,name:"midhun",email:"something@sm.com"},{id:4,name:"midhun",email:"something@sm.com"},{id:5,name:"midhun",email:"something@sm.com"},{id:6,name:"midhun",email:"something@sm.com"},{id:7,name:"midhun",email:"something@sm.com"},{id:8,name:"midhun",email:"something@sm.com"}];

//sample Data-Employees
var employees=[{fname:"lijin",lname:"thomas",email:"kl@mk.com",likes:0,dlikes:0},{fname:"lijin",lname:"thomas",email:"kl@mk.com",likes:0,dlikes:0},
    {fname:"lijin",lname:"thomas",email:"kl@mk.com",likes:0,dlikes:0},{fname:"lijin",lname:"thomas",email:"kl@mk.com",likes:0,dlikes:0},{fname:"lijin",lname:"thomas",email:"kl@mk.com",likes:0,dlikes:0},{fname:"lijin",lname:"thomas",email:"kl@mk.com",likes:0,dlikes:0},{fname:"lijin",lname:"thomas",email:"kl@mk.com",likes:0,dlikes:0},{fname:"lijin",lname:"thomas",email:"kl@mk.com",likes:0,dlikes:0},{fname:"lijin",lname:"thomas",email:"kl@mk.com",likes:0,dlikes:0}
];   
//////////////////////****************/////////////////
///////Module Definition/////////////
var myapp=angular.module("myapp",["ui.router"])
                 .config(
        function($stateProvider,$urlRouterProvider)
{
$urlRouterProvider.otherwise("Home");
$stateProvider
            .state("Home",{
                     url:"/Home",
                     templateUrl:"Home.html",
                     controller:"HomeController"
            })
            .state("Table",{
                     url:"/Table",
                     templateUrl:"Tview.html",
                     controller:"TableController"
            })
            .state("List",{
                     url:"/List",
                     templateUrl:"Lview.html",
                     controller:"ListController"
            })
            .state("Details",{
                     url:"/List/:id",
                     templateUrl:"Details.html",
                     controller:"DetailsController"
            })  
            
});

myapp.controller("HomeController",function($scope){
$scope.message="Welcome::"+new Date().toLocaleString();

});
myapp.controller("TableController",function($scope){
    $scope.employees=employees;
    $scope.nrows=3;
    $scope.vi='Table.html';

$scope.inclk=function(employe){
          employe.likes++;    
}
$scope.incdlk=function(employe){
          employe.dlikes++;    
}
});
myapp.controller("ListController",function($scope){
    $scope.studlist=students;
});

myapp.controller("DetailsController",function($scope,$stateParams){
    var item,temp;
    $scope.details=students[$stateParams.id-1];
});